﻿//簡単に通信機能を実装するためのクラス
//MyServer rev.20180605
#define _DEBUG_
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net.Sockets;

namespace Bel.Net
{
    public class MyServer
    {
        public delegate void ReadEventHandler(object sender, ReadEventArgs e);
        public event EventHandler NetworkConnected;
        public event EventHandler NetworkDisconnected;
        public event ReadEventHandler DataReceived;
        public List<ClientElement> cls;

        private Dictionary<int, ReadEventHandler> _dataReceivers;
        private event ReadEventHandler BroadcastReceived;
        private event ReadEventHandler ForwardReceived;
        private bool _netExit;
        private readonly TcpListener _listener;
        private readonly string _key;
        private readonly Thread _workingThread;


        public MyServer(int port, string key)
        {
#if _DEBUG_
            NetworkConnected += connected;
            NetworkDisconnected += disconnected;
#endif
            DataReceived += received;
            BroadcastReceived += BcReceived;
            ForwardReceived += FrReceived;
            NetworkConnected += ClsSend;
            NetworkDisconnected += ClsSend;
            _dataReceivers = new Dictionary<int, ReadEventHandler>();
            cls = new List<ClientElement>();
            _key = key;
            _netExit = false;
            _listener = new TcpListener(port);
            _listener.Start();
            _listener.BeginAcceptTcpClient(new AsyncCallback(DoAcceptTcpClientCallback), _listener);
            _workingThread = new Thread(new ThreadStart(Working));
        }

        public void Start()
        {
            _workingThread.Start();
        }

        public bool Write(int header, byte[] data, string receiverName)
        {
            if (receiverName == "annms" || receiverName == "Host")
                return false;
            else
            {
                bool result = false;
                foreach (ClientElement clientElement in cls)
                {
                    if (clientElement.Name == receiverName)
                    {
                        _write(header, clientElement.Uuid, Header.Bytes, Convert.ToBase64String(data));
                        result = true;
                    }
                }
                return result;
            }
        }

        public bool Write(int header, string str, string receiverName)
        {
            if (receiverName == "annms" || receiverName == "Host")
                return false;
            else
            {
                bool result = false;
                foreach (ClientElement clientElement in cls)
                {
                    if (clientElement.Name == receiverName)
                    {
                        _write(header, clientElement.Uuid, Header.Text, str);
                        result = true;
                    }
                }
                return result;
            }
        }

        public bool Write(int header, List<string> strs, string receiverName)
        {
            if (receiverName == "annms" || receiverName == "Host")
                return false;
            else
            {
                bool result = false;
                foreach (ClientElement clientElement in cls)
                {
                    if (clientElement.Name == receiverName)
                    {
                        _writeList(header, clientElement.Uuid, Header.ListText, strs);
                        result = true;
                    }
                }
                return result;
            }
        }

        public void WriteUuid(int header, byte[] data, string addr)
        {
            if (addr == "annms")
                return;
            _write(header, addr, Header.Bytes, Convert.ToBase64String(data));
        }

        public void WriteUuid(int header, string str, string addr)
        {
            if (addr == "annms")
                return;
            _write(header, addr, Header.Text, str);
        }

        public void WriteUuid(int header, List<string> strs, string addr)
        {
            if (addr == "annms")
                return;
            _writeList(header, addr, Header.ListText, strs);
        }

        public void WriteBroadcast(int header, byte[] data)
        {

            foreach (ClientElement cl in cls)
            {
                cl.WriteStrs.Add((int)Header.BroadcastBytes + " " + header + " " + Convert.ToBase64String(data));
            }
        }

        public void WriteBroadcast(int header, string str)
        {

            foreach (ClientElement cl in cls)
            {
                cl.WriteStrs.Add((int)Header.BroadcastText + " " + header + " " + str);
            }
        }

        public void WriteBroadcast(int header, List<string> strs)
        {
            string uuid = Guid.NewGuid().ToString("N");
            foreach (ClientElement cl in cls)
            {
                foreach (string str in strs)
                    cl.WriteStrs.Add((int)Header.ListBroadcastText + " " + header + " " + uuid + " " + str);
                cl.WriteStrs.Add((int)Header.ListBroadcastEnd + " " + header + " " + uuid + " ");
            }
        }

        public void SetDataReceiver(int header, ReadEventHandler eventHandler)
        {
            _dataReceivers.Add(header, eventHandler);
        }

        public void Dispose()
        {
            _netExit = true;
            if (_workingThread != null) _workingThread.Abort();
            if (_listener != null) _listener.Stop();
            foreach (ClientElement cl in cls)
            {
                cl.Dispose();
            }
            GC.SuppressFinalize(true);
        }

        private void Working()
        {
            bool noSleepFlag;
            while (!_netExit)
            {
                noSleepFlag = false;
                if (cls.Count != 0)
                {
                    for (int i = 0; i < cls.Count; i++)
                    {
                        if (cls[i].ConnectionClosed)
                            cls.RemoveAt(i);
                    }
                    foreach (ClientElement cl in cls)
                    {
                        if (cl.IsConnected && !_netExit)
                        {
                            if (cl.Client.Available != 0)
                                cl.TryRead();
                            if (cl.WriteStrs.Count != 0)
                                cl.TryWrite();
                            if (cl.Client.Available != 0 || cl.WriteStrs.Count != 0)
                                noSleepFlag = true;
                        }
                        else
                        {
                            cl.Dispose();
                        }
                    }
                    if (!noSleepFlag)
                        Thread.Sleep(50);
                }
                else
                    Thread.Sleep(200);
            }
        }

        private void _write(int header, string addr, Header h, string str)
        {
            //Header Header Sender Data
            foreach (ClientElement cl in cls)
            {
                if (cl.Uuid == addr)
                    cl.WriteStrs.Add((int)h + " " + header + " " + "Host" + " " + str);
            }
        }

        private void _writeList(int header, string addr, Header h, List<string> strs)
        {
            //RcvUUIFD Header ListUUID Header Sender Str
            string uuid = Guid.NewGuid().ToString("N");
            foreach (ClientElement cl in cls)
            {
                if (cl.Uuid == addr)
                {
                    foreach (string str in strs)
                        cl.WriteStrs.Add((int)h + " " + header + " " + uuid + " " + str);
                    if (h == Header.ListText)
                        cl.WriteStrs.Add((int)Header.ListTextEnd + " " + header + " " + uuid + " Host");
                    else if (h == Header.ListBroadcastText)
                        cl.WriteStrs.Add((int)Header.ListBroadcastEnd + " " + header + " " + uuid);
                }
            }
        }

        private void DoAcceptTcpClientCallback(IAsyncResult aresult)
        {
            TcpClient cl = null;
            TcpListener listener = (TcpListener)aresult.AsyncState;
            try
            {
                cl = listener.EndAcceptTcpClient(aresult);
            }
            catch (Exception)
            {
                return;
            }
            cls.Add(new ClientElement(cl,
                NetworkConnected, NetworkDisconnected, DataReceived, BroadcastReceived, ForwardReceived, _key));
            listener.BeginAcceptTcpClient(new AsyncCallback(DoAcceptTcpClientCallback), listener);
        }

        private void ClsSend(object sender, EventArgs e)
        {
            string sendData = "";
            foreach (ClientElement cl in cls)
            {
                sendData += cl.Name + " " + cl.Uuid + " ";
            }
            foreach (ClientElement cl in cls)
            {
                cl.WriteStrs.Add((int)Header.Cls + " " + sendData);
            }
        }

        private void BcReceived(object sender, ReadEventArgs e)
        {
            foreach (ClientElement cl in cls)
            {
                if (cl != sender as ClientElement)
                {
                    switch (e.netHeader)
                    {
                        case Header.BroadcastBytes:
                            cl.WriteStrs.Add((int)Header.BroadcastBytes + " " + e.Header + " " + Convert.ToBase64String(e.Bytes));
                            break;
                        case Header.BroadcastText:
                            cl.WriteStrs.Add((int)Header.BroadcastText + " " + e.Header + " " + e.Text);
                            break;
                        case Header.ListBroadcastEnd:
                            string uuid = Guid.NewGuid().ToString("N");
                            foreach (string str in e.Texts)
                                cl.WriteStrs.Add((int)Header.ListBroadcastText + " " + e.Header + " " + uuid + " " + str);
                            cl.WriteStrs.Add((int)Header.ListBroadcastEnd + " " + e.Header + " " + uuid + " ");
                            break;
                    }
                }
            }
#if _DEV_
            Console.WriteLine("Bloadcast received: " + e.Text);
#endif
        }

        private void FrReceived(object sender, ReadEventArgs e)
        {
            foreach (ClientElement cl in cls)
            {
                if(cl.Uuid == sender as string)
                switch (e.netHeader)
                {
                    case Header.Bytes:
                        cl.WriteStrs.Add((int)Header.Bytes + " " + e.Header + " " + e.Sender + " " + Convert.ToBase64String(e.Bytes));
                        break;
                    case Header.Text:
                        cl.WriteStrs.Add((int)Header.Text + " " + e.Header + " " + e.Sender + " " + e.Text);
                        break;
                    case Header.ListTextEnd:
                        string uuid = Guid.NewGuid().ToString("N");
                        foreach (string str in e.Texts)
                            cl.WriteStrs.Add((int)Header.ListText + " " + e.Header + " " + e.Sender + " " + uuid + " " + str);
                        cl.WriteStrs.Add((int)Header.ListTextEnd + " " + e.Header + " " + e.Sender + " " + uuid + " ");
                        break;
                }
            }
        }

#if _DEBUG_
        private void connected(object sender, EventArgs e)
        {
            var netClient = sender as ClientElement;
            if (netClient != null)
                Console.WriteLine("Connected To: " + netClient.Name
                                  + "\n" + "\tUUID: " + netClient.Uuid);
        }

        private void disconnected(object sender, EventArgs e)
        {
            var netClient = sender as ClientElement;
            if (netClient != null)
                Console.WriteLine("Disconnected from:\n\t" + netClient.Name
                                  + "\n" + "\tSENDER UUID:" + netClient.Uuid);
        }
#endif
        private void received(object sender, ReadEventArgs e)
        {
#if _DEBUG_
            var netClient = sender as ClientElement;
            if (netClient != null)
            {
                if (e.Text.Length >= 100)
                {
                    Console.WriteLine("NET RECEIVED:\n\tSENDER NAME: " + netClient.Name
                                      + "\n" + "\tSENDER UUID: " + netClient.Uuid + "\n\tTXT: " + e.Text.Substring(0, 100) + "...");
                }
                else
                    Console.WriteLine("NET RECEIVED:\n\tSENDER NAME: " + netClient.Name
                        + "\n" + "\tSENDER UUID: " + netClient.Uuid + "\n\tTXT: " + e.Text);
            }
#endif
            if (_dataReceivers.ContainsKey(e.Header))
                _dataReceivers[e.Header](sender, e);
        }
    }

    public class ClientElement
    {
        public string Uuid = "annms";
        public string Name = "annms";
        public bool IsConnected = false;
        public event EventHandler NetworkConnected;
        public event EventHandler NetworkDisconnected;
        public event MyServer.ReadEventHandler ForwardReceived;
        public event MyServer.ReadEventHandler BroadcastReceived;
        public event MyServer.ReadEventHandler DataReceived;
        public List<string> WriteStrs;
        public List<string> ListUuids;
        public Dictionary<string, List<string>> ListStrs;
        public TcpClient Client = null;
        public bool ConnectionClosed = false;

        private byte[] NextData;
        private readonly string _key;
        private readonly Encoding enc = Encoding.UTF8;
        private Timer _pingTimer = null;
        private NetworkStream _netstream = null;
        private bool _initialized = false;
        public ClientElement(TcpClient client, EventHandler connected, EventHandler disconnected,
            MyServer.ReadEventHandler received, MyServer.ReadEventHandler bcReceived, MyServer.ReadEventHandler frReceived, string key)
        {
            NetworkConnected += connected;
            NetworkDisconnected += disconnected;
            DataReceived += received;
            BroadcastReceived += bcReceived;
            ForwardReceived += frReceived;
            this.Client = client;
            _key = key;
            WriteStrs = new List<string>();
            ListUuids = new List<string>();
            ListStrs = new Dictionary<string, List<string>>();
            ConnectionFinalize();
        }

        public void Dispose()
        {
            if (_netstream != null) _netstream.Close();
            if (Client != null) Client.Close();
            ConnectionClosed = true;
        }

        public bool TryWrite(byte[] sendBytes)
        {
            _pingTimer.Change(3000, 3000);
            bool compressed = false;
            if (sendBytes.Length > 100000)
            {
                //100KiB以上なら圧縮
                sendBytes = Base.ConvertByteCompress(sendBytes);
                compressed = true;
            }
            if (!_initialized)
                return false;
            List<byte> newBytes = new List<byte>();
            byte zeroCount = 0;
            foreach (byte b in sendBytes)
            {
                if (b == 0)
                {
                    if (zeroCount == 255)
                    {
                        newBytes.Add(0);
                        newBytes.Add(255);
                        zeroCount = 0;
                    }
                    zeroCount++;
                }
                else if (zeroCount != 0)
                {
                    newBytes.Add(0);
                    newBytes.Add(zeroCount);
                    newBytes.Add(b);
                    zeroCount = 0;
                }
                else
                    newBytes.Add(b);
            }
            if (zeroCount != 0)
            {
                newBytes.Add(0);
                newBytes.Add(zeroCount);
            }
            newBytes.Add(0);
            if (compressed)
                newBytes.Add(255);
            else
                newBytes.Add(0);
            sendBytes = newBytes.ToArray();

            try
            {
                _netstream.Write(sendBytes, 0, sendBytes.Length);
            }
            catch (Exception)
            {
                IsConnected = false;
                _initialized = false;
                if (NetworkDisconnected != null) NetworkDisconnected(this, EventArgs.Empty);
                return false;
            }

#if _DEBUG_

            if (WriteStrs.Count != 0)
            {
                if (WriteStrs[0].Length >= 100)
                    Console.WriteLine("NET SEND:\n\tTo:" + Name + "\n\tText:" + WriteStrs[0].Substring(0, 100) + "...");
                else
                    Console.WriteLine("NET SEND:\n\tTo:" + Name + "\n\tText:" + WriteStrs[0]);
            }
#endif
            if (WriteStrs.Count != 0)
                WriteStrs.RemoveAt(0);
            return true;
        }

        public bool TryWrite()
        {
            if (!_initialized)
                return false;

            bool compressed = false;
            _pingTimer.Change(3000, 3000);
            byte[] sendBytes = Base.EncryptString(WriteStrs[0], _key);
            if (sendBytes.Length > 100000)
            {
                //100KiB以上なら圧縮
                sendBytes = Base.ConvertByteCompress(sendBytes);
                compressed = true;
            }
            List<byte> newBytes = new List<byte>();
            byte zeroCount = 0;
            foreach (byte b in sendBytes)
            {
                if (b == 0)
                {
                    if (zeroCount == 254)
                    {
                        newBytes.Add(0);
                        newBytes.Add(254);
                        zeroCount = 0;
                    }
                    zeroCount++;
                }
                else if (zeroCount != 0)
                {
                    newBytes.Add(0);
                    newBytes.Add(zeroCount);
                    newBytes.Add(b);
                    zeroCount = 0;
                }
                else
                    newBytes.Add(b);
            }
            if (zeroCount != 0)
            {
                newBytes.Add(0);
                newBytes.Add(zeroCount);
            }
            newBytes.Add(0);
            if (compressed)
                newBytes.Add(255);
            else
                newBytes.Add(0);
            sendBytes = newBytes.ToArray();

            try
            {
                _netstream.Write(sendBytes, 0, sendBytes.Length);
            }
            catch (Exception)
            {
                IsConnected = false;
                _initialized = false;
                if (NetworkDisconnected != null) NetworkDisconnected(this, EventArgs.Empty);
                return false;
            }

#if _DEBUG_

            if (WriteStrs.Count != 0)
            {
                if (WriteStrs[0].Length >= 100)
                    Console.WriteLine("NET SEND:\n\tTo:" + Name + "\n\tText:" + WriteStrs[0].Substring(0, 100) + "...");
                else
                    Console.WriteLine("NET SEND:\n\tTo:" + Name + "\n\tText:" + WriteStrs[0]);
            }
#endif
            if (WriteStrs.Count != 0)
                WriteStrs.RemoveAt(0);
            return true;
        }

        private int find0Footer(System.IO.MemoryStream ms)
        {
            bool flag = false;
            int pos = 0;
            foreach (byte b in ms.ToArray())
            {
                if (b == 0)
                {
                    if (flag)
                        return pos;
                    else
                        flag = true;
                }
                else if (flag && b == 255)
                {
                    return pos;
                }
                else
                    flag = false;
                pos++;
            }
            return 0;
        }

        private int find0Footer(byte[] bs)
        {
            bool flag = false;
            int pos = 0;
            foreach (byte b in bs)
            {
                if (b == 0)
                {
                    if (flag)
                        return pos;
                    else
                        flag = true;
                }
                else if (flag && b == 255)
                {
                    return pos;
                }
                else
                    flag = false;
                pos++;
            }
            return 0;
        }

        public void TryRead()
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            byte[] resBytes = new byte[256];
            var resSize = 0;
            if (NextData != null)
                ms.Write(NextData, 0, NextData.Length);
            do
            {
                try
                {
                    //データの一部を受信する
                    resSize = _netstream.Read(resBytes, 0, resBytes.Length);
                }
                catch (Exception)
                {
                    return;
                }
                //受信したデータを蓄積する
                ms.Write(resBytes, 0, resSize);
                //ストリームにフッター00が含まれていないときは
                // 受信を続ける
            } while (resSize == 0 || find0Footer(resBytes) == 0);

            while (find0Footer(ms) != 0)
            {
                List<string> rawTexts = new List<string>();
                ReadEventArgs eventArgs = new ReadEventArgs();
                //受信したデータを文字列に変換
                List<byte> bufBytes = new List<byte>();
                byte[] getCurrentData = new byte[find0Footer(ms) - 1];
                Array.Copy(ms.ToArray(), 0, getCurrentData, 0, find0Footer(ms) - 1);

                if ((ms.ToArray().Length - find0Footer(ms) - 1) > 0)
                {
                    NextData = new byte[ms.ToArray().Length - find0Footer(ms) - 1];
                    Array.Copy(ms.ToArray(), find0Footer(ms) + 1, NextData, 0, ms.ToArray().Length - find0Footer(ms) - 1);
                }
                else
                    NextData = null;
                int count = getCurrentData.Length;
                for (int i = 0; i < count; i++)
                {
                    if (getCurrentData[i] != 0)
                        bufBytes.Add(getCurrentData[i]);
                    else
                    {
                        for (int j = 0; j < getCurrentData[i + 1]; j++)
                            bufBytes.Add(0);
                        i++;
                    }
                }
                string resMsg;
                if (ms.GetBuffer()[find0Footer(ms)] == 255)
                    resMsg = Base.DecryptString(Base.ConvertByteDecompress(bufBytes.ToArray()), _key);
                else
                    resMsg = Base.DecryptString(bufBytes.ToArray(), _key);
                ms.Close();

                //末尾の\nを削除し\nで分割
                rawTexts.AddRange(resMsg.TrimEnd('\n').Split('\n'));
                foreach (string text in rawTexts)
                {
                    Header h;
                    List<string> textElements = new List<string>(text.Split(' '));
                    string receiverUuid = textElements[0];
                    h = (Header)int.Parse(textElements[1]);
                    switch (h)
                    {
                        //textElements[2]以降は任意
                        case Header.BroadcastBytes:
                            eventArgs.netHeader = Header.BroadcastBytes;
                            eventArgs.Header = int.Parse(textElements[2]);
                            eventArgs.Text = string.Join(" ", textElements.GetRange(3, textElements.Count - 3).ToArray());
                            eventArgs.Bytes = Convert.FromBase64String(eventArgs.Text);
                            if (BroadcastReceived != null) BroadcastReceived(this, eventArgs);
                            if (DataReceived != null) DataReceived(this, eventArgs);
                            break;
                        case Header.BroadcastText:
                            eventArgs.netHeader = Header.BroadcastText;
                            eventArgs.Header = int.Parse(textElements[2]);
                            eventArgs.Text = string.Join(" ", textElements.GetRange(3, textElements.Count - 3).ToArray());
                            if (BroadcastReceived != null) BroadcastReceived(this, eventArgs);
                            if (DataReceived != null) DataReceived(this, eventArgs);
                            break;
                        case Header.Bytes:
                            eventArgs.netHeader = Header.Text;
                            eventArgs.Header = int.Parse(textElements[2]);
                            eventArgs.Sender = textElements[3];
                            eventArgs.Text = string.Join(" ", textElements.GetRange(4, textElements.Count - 4).ToArray());
                            eventArgs.Bytes = Convert.FromBase64String(eventArgs.Text);
                            if (receiverUuid == "0")
                            {
                                if (DataReceived != null) DataReceived(this, eventArgs);
                            }
                            else
                            {
                                if (ForwardReceived != null) ForwardReceived(receiverUuid, eventArgs);
                            }
                            break;
                        case Header.Text:
                            eventArgs.netHeader = Header.Text;
                            eventArgs.Header = int.Parse(textElements[2]);
                            eventArgs.Sender = textElements[3];
                            eventArgs.Text = string.Join(" ", textElements.GetRange(4, textElements.Count - 4).ToArray());
                            if (receiverUuid == "0")
                            {
                                if (DataReceived != null) DataReceived(this, eventArgs);
                            }
                            else
                            {
                                if (ForwardReceived != null) ForwardReceived(receiverUuid, eventArgs);
                            }
                            break;
                        case Header.ListBroadcastText:
                            //ReceiverUUID Header Header ListUUID Str
                            if (!ListUuids.Contains(textElements[3]))
                            {
                                ListUuids.Add(textElements[3]);
                                ListStrs.Add(textElements[3], new List<string>());
                                ListStrs[textElements[3]].Add(string.Join(" ",
                                    textElements.GetRange(4, textElements.Count - 4).ToArray()));
                            }
                            else
                            {
                                ListStrs[textElements[3]].Add(string.Join(" ",
                                    textElements.GetRange(4, textElements.Count - 4).ToArray()));
                            }
                            eventArgs.netHeader = Header.ListBroadcastText;
                            eventArgs.Header = int.Parse(textElements[2]);
                            eventArgs.Text = string.Join(" ",
                                textElements.GetRange(3, textElements.Count - 3).ToArray());
                            break;
                        case Header.ListText:
                            //ReceiverUUID Header Header ListUUID Str
                            if (receiverUuid == "0")
                            {
                                if (!ListUuids.Contains(textElements[3]))
                                {
                                    ListUuids.Add(textElements[3]);
                                    ListStrs.Add(textElements[3], new List<string>());
                                    ListStrs[textElements[3]].Add(string.Join(" ",
                                        textElements.GetRange(4, textElements.Count - 4).ToArray()));
                                }
                                else
                                {
                                    ListStrs[textElements[3]].Add(string.Join(" ",
                                        textElements.GetRange(4, textElements.Count - 4).ToArray()));
                                }
                            }
                            else
                            {
                                eventArgs.netHeader = Header.ListText;
                                eventArgs.Header = int.Parse(textElements[2]);
                                eventArgs.Text = string.Join(" ", textElements.GetRange(3, textElements.Count - 3).ToArray());
                            }
                            break;
                        case Header.ListBroadcastEnd:
                            //ReceiverUUID Header Header ListUUID
                            eventArgs.netHeader = Header.ListBroadcastEnd;
                            eventArgs.Header = int.Parse(textElements[2]);
                            eventArgs.Text = textElements[3];
                            ListUuids.Remove(textElements[3]);
                            if (BroadcastReceived != null) BroadcastReceived(this, eventArgs);
                            eventArgs.Texts = ListStrs[textElements[3]];
                            ListStrs.Remove(textElements[3]);
                            if (DataReceived != null) DataReceived(this, eventArgs);
                            break;
                        case Header.ListTextEnd:
                            //ReceiverUUID Header Header ListUUID Sender
                            if (receiverUuid == "0")
                            {
                                eventArgs.netHeader = Header.ListTextEnd;
                                eventArgs.Header = int.Parse(textElements[2]);
                                eventArgs.Texts = ListStrs[textElements[3]];
                                eventArgs.Sender = textElements[4];
                                ListUuids.Remove(textElements[3]);
                                ListStrs.Remove(textElements[3]);
                                if (DataReceived != null) DataReceived(this, eventArgs);
                            }
                            else
                            {
                                eventArgs.netHeader = Header.ListTextEnd;
                                eventArgs.Header = int.Parse(textElements[2]);
                                eventArgs.Text = textElements[3];
                                eventArgs.Sender = textElements[4];
                                if (ForwardReceived != null) ForwardReceived(receiverUuid, eventArgs);
                            }
                            break;
                        case Header.UuidRequest:
                            Uuid = Guid.NewGuid().ToString("N");
                            Name = string.Join(" ", textElements.GetRange(2, textElements.Count - 2).ToArray());
                            WriteStrs.Add((int)Header.UuidRequest + " " + Uuid);
                            _initialized = true;
                            if (NetworkConnected != null) NetworkConnected(this, EventArgs.Empty);
                            break;
                        default:
                            break;
                    }
                }
                ms = new System.IO.MemoryStream();
                if (NextData != null)
                    ms.Write(NextData, 0, NextData.Length);
            }
        }

        private void ConnectionFinalize()
        {
            _netstream = Client.GetStream();
            _netstream.ReadTimeout = 10000;
            _pingTimer = new Timer(new TimerCallback(TimerCallBack));
            IsConnected = true;
        }

        private void TimerCallBack(object state)
        {
            _pingTimer.Change(Timeout.Infinite, 3000);
            TryWrite(Base.EncryptString("0 " + (int)Header.Ping, _key));
        }
    }
}
