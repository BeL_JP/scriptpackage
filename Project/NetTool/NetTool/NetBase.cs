﻿//簡単に通信機能を実装するためのクラス
//NetBase rev.20170622
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bel.Net
{
    public enum Header { Ping, UuidRequest, Bytes, Text, ListText, ListTextEnd, BroadcastBytes, BroadcastText, ListBroadcastText, ListBroadcastEnd, Cls };

    public class ReadEventArgs : EventArgs
    {
        public List<string> Texts = new List<string>();

        public string Text
        {
            get { return Texts[0]; }
            set
            {
                if (Texts.Count == 0)
                    Texts.Add(value);
                else
                    Texts[0] = value;
            }
        }
        public string Sender = "annms";
        public int Header;
        public byte[] Bytes;
        public Header netHeader;
    }

    public static class Base
    {
        public static string ImgToBase64(string imgDir)
        {
            System.Drawing.Image img = new System.Drawing.Bitmap(imgDir);
            MemoryStream ms = new MemoryStream();
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] imgBytes = ms.GetBuffer();
            return Convert.ToBase64String(imgBytes);
        }

        public static System.Drawing.Image Base64ToImg(string data)
        {
            byte[] bs = Convert.FromBase64String(data);
            System.Drawing.ImageConverter imgconv = new System.Drawing.ImageConverter();
            System.Drawing.Image img = (System.Drawing.Image)imgconv.ConvertFrom(bs);
            return img;
        }

        /// <summary>
        /// 文字列を暗号化する
        /// </summary>
        /// <param name="sourceString">暗号化する文字列</param>
        /// <param name="password">暗号化に使用するパスワード</param>
        /// <returns>暗号化された文字列</returns>
        public static byte[] EncryptString(string sourceString, string password)
        {
            //RijndaelManagedオブジェクトを作成
            System.Security.Cryptography.RijndaelManaged rijndael =
                new System.Security.Cryptography.RijndaelManaged();

            //パスワードから共有キーと初期化ベクタを作成
            byte[] key, iv;
            GenerateKeyFromPassword(
                password, rijndael.KeySize, out key, rijndael.BlockSize, out iv);
            rijndael.Key = key;
            rijndael.IV = iv;

            //文字列をバイト型配列に変換する
            byte[] strBytes = Encoding.UTF8.GetBytes(sourceString);

            //対称暗号化オブジェクトの作成
            System.Security.Cryptography.ICryptoTransform encryptor =
                rijndael.CreateEncryptor();
            //バイト型配列を暗号化する
            byte[] encBytes = encryptor.TransformFinalBlock(strBytes, 0, strBytes.Length);
            //閉じる
            encryptor.Dispose();

            //バイト型配列を文字列に変換して返す
            return encBytes;
        }

        /// <summary>
        /// 暗号化されたバイト列を復号化する
        /// </summary>
        /// <param name="sourceBytes">暗号化されたバイト列</param>
        /// <param name="password">暗号化に使用したパスワード</param>
        /// <returns>復号化された文字列</returns>
        public static string DecryptString(byte[] sourceBytes, string password)
        {
            //RijndaelManagedオブジェクトを作成
            System.Security.Cryptography.RijndaelManaged rijndael =
                new System.Security.Cryptography.RijndaelManaged();

            //パスワードから共有キーと初期化ベクタを作成
            byte[] key, iv;
            GenerateKeyFromPassword(
                password, rijndael.KeySize, out key, rijndael.BlockSize, out iv);
            rijndael.Key = key;
            rijndael.IV = iv;

            //対称暗号化オブジェクトの作成
            System.Security.Cryptography.ICryptoTransform decryptor =
                rijndael.CreateDecryptor();
            //バイト型配列を復号化する
            //復号化に失敗すると例外CryptographicExceptionが発生
            byte[] decBytes = decryptor.TransformFinalBlock(sourceBytes, 0, sourceBytes.Length);
            //閉じる
            decryptor.Dispose();

            //バイト型配列を文字列に戻して返す
            return Encoding.UTF8.GetString(decBytes);
        }

        /// <summary>
        /// バイト列を圧縮する
        /// </summary>
        /// <param name="inbyte">圧縮するバイト列</param>
        /// <returns>圧縮されたバイト列</returns>
        public static byte[] ConvertByteCompress(byte[] inbyte)
        {
            var ms = new System.IO.MemoryStream();
            var CompressedStream = new System.IO.Compression.DeflateStream(ms,
            System.IO.Compression.CompressionMode.Compress, true);
            CompressedStream.Write(inbyte, 0, inbyte.Length);
            CompressedStream.Close();
            byte[] destination = ms.ToArray();
            ms.Close();
            return destination;
        }

        /// <summary>
        /// 圧縮されたバイト列を展開する
        /// </summary>
        /// <param name="inbyte">圧縮されたバイト列</param>
        /// <returns>展開されたバイト列</returns>
        public static byte[] ConvertByteDecompress(byte[] inbyte)
        {
            var ms = new System.IO.MemoryStream(inbyte);
            var ms2 = new System.IO.MemoryStream();
            var CompressedStream = new System.IO.Compression.DeflateStream(ms,
            System.IO.Compression.CompressionMode.Decompress, true);
                while (true)
            {
                int rb = CompressedStream.ReadByte();
                // 読み終わったとき while 処理を抜けます 
                if (rb == -1)
                {
                    break;
                }
                // メモリに展開したデータを読み込みます 
                ms2.WriteByte((byte)rb);
            }
            CompressedStream.Close();
            byte[] destination = ms2.ToArray();
            ms.Close();
            ms2.Close();
            return destination;
        }

        /// <summary>
        /// パスワードから共有キーと初期化ベクタを生成する
        /// </summary>
        /// <param name="password">基になるパスワード</param>
        /// <param name="keySize">共有キーのサイズ（ビット）</param>
        /// <param name="key">作成された共有キー</param>
        /// <param name="blockSize">初期化ベクタのサイズ（ビット）</param>
        /// <param name="iv">作成された初期化ベクタ</param>
        private static void GenerateKeyFromPassword(string password,
            int keySize, out byte[] key, int blockSize, out byte[] iv)
        {
            //パスワードから共有キーと初期化ベクタを作成する
            //saltを決める
            byte[] salt = Encoding.UTF8.GetBytes("qBQwBTzaC2R1qWE1mQdrbfvMLGQMd6LeSfNsCZYM");
            //Rfc2898DeriveBytesオブジェクトを作成する
            System.Security.Cryptography.Rfc2898DeriveBytes deriveBytes =
                new System.Security.Cryptography.Rfc2898DeriveBytes(password, salt);
            //.NET Framework 1.1以下の時は、PasswordDeriveBytesを使用する
            //System.Security.Cryptography.PasswordDeriveBytes deriveBytes =
            //    new System.Security.Cryptography.PasswordDeriveBytes(password, salt);
            //反復処理回数を指定する デフォルトで1000回
            deriveBytes.IterationCount = 1000;

            //共有キーと初期化ベクタを生成する
            key = deriveBytes.GetBytes(keySize / 8);
            iv = deriveBytes.GetBytes(blockSize / 8);
        }
    }
}
