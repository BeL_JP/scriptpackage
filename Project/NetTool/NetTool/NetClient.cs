﻿//簡単に通信機能を実装するためのクラス
//NetClient rev.20170622
#define _DEBUG_
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Linq;

namespace Bel.Net
{
    public class MyClient : IDisposable
    {
        public bool IsConnected { get; protected set; }
        public string Uuid { get; protected set; }
        public event EventHandler NetworkConnected;
        public event EventHandler NetworkDisconnected;
        public event ReadEventHandler DataReceived;
        public event EventHandler ClientDataReceived;
        public delegate void ReadEventHandler(object sender, ReadEventArgs e);

        private Dictionary<int,ReadEventHandler> _dataReceivers;
        private readonly IPAddress _ipadr;
        private readonly int _port;
        private TcpClient _client;
        private NetworkStream _netstream;
        private System.Threading.Thread _connectClientThread;
        private System.Threading.Thread _workingThread;
        private List<string> _writeStrs;
        private List<string> _listUuids;
        private Dictionary<string, List<string>> _listStrs;
        private Timer _pingTimer;
        private Clients _clsData;
        private readonly string _clientName;
        private readonly Encoding _enc;
        private readonly string _key;
        private bool _exit;
        private byte[] NextData;

        public MyClient(string ipStr, int port,string clientName, string key)
        {
#if _DEBUG_
            NetworkConnected += connected;
#endif
            DataReceived += received;
            _clientName = clientName;
            _key = key;
            NetworkDisconnected += Disconnected;
            _dataReceivers = new Dictionary<int, ReadEventHandler>();
            _ipadr = IPAddress.Parse(ipStr);
            _port = port;
            _enc=Encoding.UTF8;
            IsConnected = false;
            Uuid = "annms";
            _exit = false;
        }

        public void Start()
        {
            BeginConnect();
        }

        public void WaitConnection()
        {
            while (!IsConnected)
            {
                System.Threading.Thread.Sleep(250);
            }
        }

        public bool Write(int header, byte[] data, string receiverName)
        {
            if (receiverName == "annms")
                return false;
            if (receiverName == "Host")
            {
                _write(header, "0", Header.Bytes, Convert.ToBase64String(data));
                return true;
            }
            else
            {
                bool result = false;
                foreach (ClientData clientData in _clsData.Cls)
                {
                    if (clientData.Name == receiverName)
                    {
                        _write(header, clientData.Uuid, Header.Bytes, Convert.ToBase64String(data));
                        result = true;
                    }
                }
                return result;
            }
        }

        public bool Write(int header, string str, string receiverName)
        {
            if (receiverName == "annms")
                return false;
            if (receiverName == "Host")
            {
                _write(header, "0", Header.Text, str);
                return true;
            }
            else
            {
                bool result = false;
                foreach (ClientData clientData in _clsData.Cls)
                {
                    if (clientData.Name == receiverName)
                    {
                        _write(header, clientData.Uuid, Header.Text, str);
                        result = true;
                    }
                }
                return result;
            }
        }

        public bool Write(int header, List<string> strs, string receiverName)
        {
            if (receiverName == "annms")
                return false;
            if (receiverName == "Host")
            {
                _writeList(header, "0", Header.ListText, strs);
                return true;
            }
            else
            {
                bool result = false;
                foreach (ClientData clientData in _clsData.Cls)
                {
                    if (clientData.Name == receiverName)
                    {
                        _writeList(header, clientData.Uuid, Header.ListText, strs);
                        result = true;
                    }
                }
                return result;
            }
        }

        public void WriteUuid(int header, byte[] data, string addr)
        {
            if (addr == "annms")
                return;
            _write(header, addr, Header.Bytes, Convert.ToBase64String(data));
        }

        public void WriteUuid(int header, string str, string addr)
        {
            if (addr == "annms")
                return;
            _write(header, addr, Header.Text, str);
        }

        public void WriteUuid(int header, List<string> strs, string addr)
        {
            if (addr == "annms")
                return;
            _writeList(header, addr, Header.ListText, strs);
        }

        public void WriteBroadcast(int header, byte[] data)
        {
            _write(header, "0", Header.BroadcastBytes, Convert.ToBase64String(data));
        }

        public void WriteBroadcast(int header, string str)
        {
            _write(header, "0", Header.BroadcastText, str);
        }

        public void WriteBroadcast(int header, List<string> strs)
        {
            _writeList(header, "0", Header.ListBroadcastText, strs);
        }

        public void SetDataReceiver(int header, ReadEventHandler eventHandler)
        {
            _dataReceivers.Add(header,eventHandler);
        }

        public void Dispose()
        {
            _exit = true;

            if (_connectClientThread != null) _connectClientThread.Abort();
            if (_workingThread != null) _workingThread.Abort();
            if (_netstream != null) _netstream.Close();
            if (_client != null) _client.Close();
        }

        private void _write(int header,string addr, Header h,string str)
        {
            _writeStrs.Add(addr + " " + (int)h + " " + header + " " + _clientName + " " + str);
        }

        private void _writeList(int header, string addr, Header h, List<string> strs)
        {
            //RcvUUID NetHeader ListUUID Header Str
            string uuid = Guid.NewGuid().ToString("N");
            foreach (string str in strs)
                _writeStrs.Add(addr + " " + (int)h + " " + header + " " + uuid + " " + str);
            switch (h)
            {
                case Header.ListText:
                    _writeStrs.Add(addr + " " + (int)Header.ListTextEnd + " " + header + " " + uuid + " " + _clientName);
                    break;
                case Header.ListBroadcastText:
                    _writeStrs.Add(addr + " " + (int)Header.ListBroadcastEnd + " " + header + " " + uuid);
                    break;
            }
        }

        private void ResetConnection()
        {
            _clsData = new Clients();
            _writeStrs = new List<string>();
            _listUuids = new List<string>();
            _listStrs = new Dictionary<string, List<string>>();
            if (_connectClientThread != null) _connectClientThread.Abort();
            if (_netstream != null) _netstream.Close();
            if (_client != null) _client.Close();
        }

        private void BeginConnect()
        {
            ResetConnection();
            _connectClientThread = new Thread(new ThreadStart(ConnectT));
            _connectClientThread.Start();
        }


        private void ConnectT()
        {
            while (!_exit)
            {
                try
                {
                    _client = new TcpClient(_ipadr.ToString(), _port);
                }
                catch
                {
                    // ignored
                }
                Thread.Sleep(250);
                if (_client != null && _client.Connected)
                    break;
            }
            if (_client != null && _client.Connected)
                ConnectionFinalize();
        }

        private void ConnectionFinalize()
        {
            _netstream = _client.GetStream();
            _netstream.ReadTimeout = 10000;
            _pingTimer = new Timer(new TimerCallback(TimerCallBack));
            Working();
            IsConnected = true;
            if (NetworkConnected != null) NetworkConnected(null, EventArgs.Empty);
        }

        private void Working()
        {
            _workingThread = new Thread(new ThreadStart(WorkingT));
            _writeStrs.Add("0 " + (int)Header.UuidRequest + " " + _clientName);
            _workingThread.Start();
        }

        private void WorkingT()
        {
            Thread.Sleep(500);
            _pingTimer.Change(3000, 3000);
            while (IsConnected && !_exit)
            {
                if (_client.Available != 0)
                    TryRead();
                if (_writeStrs.Count != 0)
                {
                    TryWrite();
                }
                if (_client.Available == 0 && _writeStrs.Count == 0)
                    Thread.Sleep(50);
            }
            _pingTimer.Change(Timeout.Infinite, 3000);
            if (NetworkDisconnected != null) NetworkDisconnected(null, EventArgs.Empty);
        }

        private bool TryWrite(byte[] sendBytes)
        {
            _pingTimer.Change(3000, 3000);
            bool compressed = false;
            if (sendBytes.Length > 100000)
            {
                //100KiB以上なら圧縮
                sendBytes = Base.ConvertByteCompress(sendBytes);
                compressed = true;
            }
            List<byte> newBytes = new List<byte>();
            byte zeroCount = 0;
            foreach (byte b in sendBytes)
            {
                if (b == 0)
                {
                    if (zeroCount == 255)
                    {
                        newBytes.Add(0);
                        newBytes.Add(255);
                        zeroCount = 0;
                    }
                    zeroCount++;
                }
                else if (zeroCount != 0)
                {
                    newBytes.Add(0);
                    newBytes.Add(zeroCount);
                    newBytes.Add(b);
                    zeroCount = 0;
                }
                else
                    newBytes.Add(b);
            }
            if (zeroCount != 0)
            {
                newBytes.Add(0);
                newBytes.Add(zeroCount);
            }
            newBytes.Add(0);
            if(compressed)
                newBytes.Add(255);
            else
                newBytes.Add(0);
            sendBytes = newBytes.ToArray();
            try
            {
                _netstream.Write(sendBytes, 0, sendBytes.Length);
            }
            catch (Exception)
            {
                IsConnected = false;
                return false;
            }

#if _DEBUG_

            if (_writeStrs.Count != 0)
            {
                if (_writeStrs[0].Length >= 100)
                    Console.WriteLine("NET SEND:" + _writeStrs[0].Substring(0, 100) + "...");
                else
                    Console.WriteLine("NET SEND:" + _writeStrs[0]);
            }
#endif
            if (_writeStrs.Count != 0)
                _writeStrs.RemoveAt(0);
            return true;
        }

        private bool TryWrite()
        {
            bool compressed = false;
            _pingTimer.Change(3000, 3000);
            byte[] sendBytes = Base.EncryptString(_writeStrs[0], _key);
            if (sendBytes.Length > 100000)
            {
                //100KiB以上なら圧縮
                sendBytes = Base.ConvertByteCompress(sendBytes);
                compressed = true;
            }
            List<byte> newBytes = new List<byte>();
            byte zeroCount = 0;
            foreach (byte b in sendBytes)
            {
                if (b == 0)
                {
                    if (zeroCount == 254)
                    {
                        newBytes.Add(0);
                        newBytes.Add(254);
                        zeroCount = 0;
                    }
                    zeroCount++;
                }
                else if (zeroCount != 0)
                {
                    newBytes.Add(0);
                    newBytes.Add(zeroCount);
                    newBytes.Add(b);
                    zeroCount = 0;
                }
                else
                    newBytes.Add(b);
            }
            if (zeroCount != 0)
            {
                newBytes.Add(0);
                newBytes.Add(zeroCount);
            }
            newBytes.Add(0);
            if (compressed)
                newBytes.Add(255);
            else
                newBytes.Add(0);
            sendBytes = newBytes.ToArray();


            try
            {
                _netstream.Write(sendBytes, 0, sendBytes.Length);
            }
            catch (Exception)
            {
                IsConnected = false;
                return false;
            }

#if _DEBUG_

            if (_writeStrs.Count != 0)
            {
                if (_writeStrs[0].Length >= 100)
                    Console.WriteLine("NET SEND:" + _writeStrs[0].Substring(0, 100) + "...");
                else
                    Console.WriteLine("NET SEND:" + _writeStrs[0]);
            }
#endif
            if (_writeStrs.Count != 0)
                _writeStrs.RemoveAt(0);
            return true;
        }

        private int find0Footer(System.IO.MemoryStream ms)
        {
            bool flag = false;
            int pos = 0;
            foreach (byte b in ms.ToArray())
            {
                if (b == 0)
                {
                    if (flag)
                        return pos;
                    else
                        flag = true;
                }
                else if (flag && b == 255)
                {
                    return pos;
                }
                else
                    flag = false;
                pos++;
            }
            return 0;
        }

        private int find0Footer(byte[] bs)
        {
            bool flag = false;
            int pos = 0;
            foreach (byte b in bs)
            {
                if (b == 0)
                {
                    if (flag)
                        return pos;
                    else
                        flag = true;
                }
                else if(flag && b == 255)
                {
                    return pos;
                }
                else
                    flag = false;
                pos++;
            }
            return 0;
        }

        private void TryRead()
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            byte[] resBytes = new byte[256];
            var resSize = 0;
            if (NextData != null)
                ms.Write(NextData, 0, NextData.Length);
            do
            {
                try
                {
                    //データの一部を受信する
                    resSize = _netstream.Read(resBytes, 0, resBytes.Length);
                }
                catch (Exception)
                {
                    return;
                }
                //受信したデータを蓄積する
                ms.Write(resBytes, 0, resSize);
                //ストリームにフッター00が含まれていないときは
                // 受信を続ける
            } while (resSize == 0 || find0Footer(resBytes)==0);

            while (find0Footer(ms) != 0)
            {
                List<string> rawTexts = new List<string>();
                ReadEventArgs eventArgs = new ReadEventArgs();
                //受信したデータを文字列に変換
                List<byte> bufBytes = new List<byte>();
                byte[] getCurrentData = new byte[find0Footer(ms) - 1];

                Array.Copy(ms.ToArray(), 0, getCurrentData, 0, find0Footer(ms) - 1);

                if ((ms.ToArray().Length - find0Footer(ms) - 1) > 0)
                {
                    NextData = new byte[ms.ToArray().Length - find0Footer(ms) - 1];
                    Array.Copy(ms.ToArray(), find0Footer(ms) + 1, NextData, 0, ms.ToArray().Length - find0Footer(ms) - 1);
                }
                else
                    NextData = null;
                int count = getCurrentData.Length;
                for (int i = 0; i < count; i++)
                {
                    if (getCurrentData[i] != 0)
                        bufBytes.Add(getCurrentData[i]);
                    else
                    {
                        for (int j = 0; j < getCurrentData[i + 1]; j++)
                            bufBytes.Add(0);
                        i++;
                    }
                }
                string resMsg;
                if (bufBytes.Count() == 41)
                    ;
                if (ms.GetBuffer()[find0Footer(ms)] == 255)
                    resMsg = Base.DecryptString(Base.ConvertByteDecompress(bufBytes.ToArray()), _key);
                else
                    resMsg = Base.DecryptString(bufBytes.ToArray(), _key);
                ms.Close();

                //末尾の\nを削除し\nで分割
                rawTexts.AddRange(resMsg.TrimEnd('\n').Split('\n'));
                foreach (string text in rawTexts)
                {
                    List<string> textElements = new List<string>(text.Split(' '));
                    var h = (Header)int.Parse(textElements[0]);
                    switch (h)
                    {
                        case Header.Bytes:
                            //NetHeader Header Sender Data
                            eventArgs.netHeader = h;
                            eventArgs.Header = int.Parse(textElements[1]);
                            eventArgs.Sender = textElements[2];
                            eventArgs.Text = string.Join(" ", textElements.GetRange(3, textElements.Count - 3).ToArray());
                            eventArgs.Bytes = Convert.FromBase64String(eventArgs.Text);
                            if (DataReceived != null) DataReceived(null, eventArgs);
                            break;
                        case Header.Text:
                            //NetHeader Header Sender Data
                            eventArgs.netHeader = h;
                            eventArgs.Header = int.Parse(textElements[1]);
                            eventArgs.Sender = textElements[2];
                            eventArgs.Text = string.Join(" ", textElements.GetRange(3, textElements.Count - 3).ToArray());
                            if (DataReceived != null) DataReceived(null, eventArgs);
                            break;
                        case Header.BroadcastBytes:
                            //NetHeader Header Data
                            eventArgs.netHeader = h;
                            eventArgs.Header = int.Parse(textElements[1]);
                            eventArgs.Text = string.Join(" ", textElements.GetRange(2, textElements.Count - 2).ToArray());
                            eventArgs.Bytes = Convert.FromBase64String(eventArgs.Text);
                            if (DataReceived != null) DataReceived(null, eventArgs);
                            break;
                        case Header.BroadcastText:
                            //NetHeader Header Data
                            eventArgs.netHeader = h;
                            eventArgs.Header = int.Parse(textElements[1]);
                            eventArgs.Text = string.Join(" ", textElements.GetRange(2, textElements.Count - 2).ToArray());
                            if (DataReceived != null) DataReceived(null, eventArgs);
                            break;
                        case Header.ListBroadcastText:
                        case Header.ListText:
                            //NetHeader Header ListUUID Str
                            if (!_listUuids.Contains(textElements[2]))
                            {
                                _listUuids.Add(textElements[2]);
                                _listStrs.Add(textElements[2], new List<string>());
                                _listStrs[textElements[2]].Add(string.Join(" ",
                                    textElements.GetRange(3, textElements.Count - 3).ToArray()));
                            }
                            else
                            {
                                _listStrs[textElements[2]].Add(string.Join(" ",
                                    textElements.GetRange(3, textElements.Count - 3).ToArray()));
                            }
                            break;
                        case Header.ListTextEnd:
                            //NetHeader Header ListUUID Sender
                            eventArgs.netHeader = h;
                            eventArgs.Header = int.Parse(textElements[1]);
                            eventArgs.Texts = _listStrs[textElements[2]];
                            eventArgs.Sender = textElements[3];
                            _listUuids.Remove(textElements[2]);
                            _listStrs.Remove(textElements[2]);
                            if (DataReceived != null) DataReceived(null, eventArgs);
                            break;
                        case Header.ListBroadcastEnd:
                            //NetHeader Header ListUUID
                            eventArgs.netHeader = h;
                            eventArgs.Header = int.Parse(textElements[1]);
                            eventArgs.Texts = _listStrs[textElements[2]];
                            _listUuids.Remove(textElements[2]);
                            _listStrs.Remove(textElements[2]);
                            if (DataReceived != null) DataReceived(null, eventArgs);
                            break;
                        case Header.UuidRequest:
                            Uuid = string.Join(" ", textElements.GetRange(1, textElements.Count - 1).ToArray());
                            break;
                        case Header.Cls:
                            _clsData.ReadCls(string.Join(" ", textElements.GetRange(1, textElements.Count - 1).ToArray()));
                            if (ClientDataReceived != null) ClientDataReceived(_clsData, EventArgs.Empty);
                            break;
                    }
                }
                ms = new System.IO.MemoryStream();
                if (NextData != null)
                    ms.Write(NextData, 0, NextData.Length);
            }
            
        }

#if _DEBUG_
        private void connected(object sender, EventArgs e)
        {
            Console.WriteLine("Connected!");
        }
#endif
        private void received(object sender,ReadEventArgs e)
        {
#if _DEBUG_
            if (e.Text.Length >= 100)
                Console.WriteLine("NET RECEIVED FROM " +e.Sender+ ":" + e.Text.Substring(0, 100) + "...");
            else
                Console.WriteLine("NET RECEIVED FROM " + e.Sender + ":" + e.Text);
#endif
            if (_dataReceivers.ContainsKey(e.Header))
                _dataReceivers[e.Header](sender, e);
        }


        private void Disconnected(object sender, EventArgs e)
        {
#if _DEBUG_
            Console.WriteLine("Disconnected");
#endif
            BeginConnect();
        }

        private void TimerCallBack(object state)
        {
            TryWrite(Base.EncryptString("0 " + (int)Header.Ping, _key));
        }
    }

    public class ClientData
    {
        public string Name;
        public string Uuid;
    }

    public class Clients
    {
        public List<ClientData> Cls=new List<ClientData>();

        public void ReadCls(string data)
        {
            string[] ds = data.Split(' ');
            int i = 0;
            ClientData cd = null;
            foreach (string d in ds)
            {
                if (d != "")
                {
                    if (i % 2 == 0)
                    {
                        cd = new ClientData();
                        cd.Name = d;
                        i++;
                    }
                    else
                    {
                        if (cd == null)
                            return;
                        cd.Uuid = d;
                        Cls.Add(cd);
                        i++;
                    }
                }
            }
        }
    }
}
