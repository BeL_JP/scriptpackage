﻿using System;
using System.Collections.Generic;

namespace Bel
{
    public sealed class CommandLineInterface
    {
        public Dictionary<string,string> options
        {
            get
            {
                return new Dictionary<string, string>(_options);
            }
        }
        private static CommandLineInterface commandLineInterface;
        private List<string> optionRegistory = new List<string>();
        private Dictionary<string, string> _options = new Dictionary<string, string>();
        public static CommandLineInterface GetInstance()
        {
            return commandLineInterface = new CommandLineInterface();
        }

        public void AddOption(string option)
        {
            optionRegistory.Add("--" + option);
        }

        public void Parse()
        {
            var args = Environment.GetCommandLineArgs();
            if (args.Length == 1)
                return;
            if(args.Length == 2)
            {
                _options.Add("noOption", args[1]);
                return;
            }
            for (int i = 1; i < args.Length; i++)
            {
                if (optionRegistory.Contains(args[i]) && ++i < args.Length)
                {
                    _options.Add(args[i - 1].Substring(2), args[i] );
                }
            }
        }
    }
}
