﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Optional;

namespace Bel
{
    public static class Updater
    {
        private static readonly string baseurl = "https://bitbucket.org/";

        public static Option<int> CheckUpdate(string userName, string repositoryName, int revision)
        {
            return GetAvailables(userName, repositoryName).Match(
            availables =>
            {
                if (availables.Count != 0 && availables.Max() > revision)
                    return availables.Max().SomeNotNull();
                else
                    return Option.None<int>();
            },
                () => Option.None<int>());
        }

        public static Option<List<int>> GetAvailables(string userName, string repository)
        {
            string source;
            var myurl = userName + "/" + repository + "/";
            using (System.Net.WebClient wc = new System.Net.WebClient())
            {
                wc.Encoding = System.Text.Encoding.UTF8;
                try
                {
                    source = wc.DownloadString(baseurl + myurl + "downloads/?tab=tags");
                }
                catch
                {
                    return Option.None<List<int>>();
                }
            }
            System.Text.RegularExpressions.Regex r =
                new System.Text.RegularExpressions.Regex(myurl + "get/r(.*?)[.]zip");
            System.Text.RegularExpressions.MatchCollection mc = r.Matches(source);
            return mc.Cast<System.Text.RegularExpressions.Match>().Select(m => int.Parse(m.Groups[1].Value)).ToList().SomeNotNull();
        }

        public static bool Download(string userName, string repository, int revision,string downloadFileName)
        {
            using (System.Net.WebClient wc = new System.Net.WebClient())
            {
                wc.Encoding = System.Text.Encoding.UTF8;
                try
                {
                    wc.DownloadFile(baseurl + userName + "/" + repository + "/get/r" + revision + ".zip", downloadFileName);
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        public static bool SelfUpdate(string userName, string repository, int currentRevision)
        {
            return CheckUpdate(userName, repository, currentRevision).Match(revision =>
            {
                if (Directory.Exists("temp_update/"))
                {
                    MyFile.DeleteDirectory("temp_update/");
                }
                Directory.CreateDirectory("temp_update/");
                Directory.CreateDirectory("temp_update/extract/");
                Download(userName, repository, revision, "temp_update/update.zip");
                ZipFile.ExtractToDirectory("temp_update/update.zip", "temp_update/extract/");
                var asm = Assembly.GetEntryAssembly();
                File.Move(asm.Location, "temp_update/old.exe");
                MyFile.CopyDirectory("temp_update/extract/Release"
                , new FileInfo(asm.Location).Directory.FullName, true);
                System.Diagnostics.Process.Start(asm.Location,string.Join(" ",Environment.GetCommandLineArgs()));
                Environment.Exit(0);
                Application.Exit();
                return true;
            }, () => false);
        }
    }
}
