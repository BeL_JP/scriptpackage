﻿using System;
using System.Collections.Generic;
using System.Linq;
using Optional;

namespace Bel
{
    public static class MyLinq
    {
        public static IEnumerable<T> Recur<T>(this T target, Func<T, T> func)
        {
            var p = target;
            while (true)
            {
                yield return p;
                p = func(p);
            }
        }

        public static IEnumerable<T> Get<T>(this Option<T> valOpt)
        {
            if (valOpt.HasValue)
                yield return valOpt.ValueOr(null);
        }
    }
}
