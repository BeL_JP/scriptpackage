﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optional;

namespace Bel
{
    public static class CryptoRsa
    {
        /// <summary>
        /// 公開鍵と秘密鍵を作成して返す
        /// </summary>
        /// <param name="publicKey">作成された公開鍵(XML形式)</param>
        /// <param name="privateKey">作成された秘密鍵(XML形式)</param>
        public static void CreateKeys(out string publicKey, out string privateKey)
        {
            //RSACryptoServiceProviderオブジェクトの作成
            System.Security.Cryptography.RSACryptoServiceProvider rsa =
                new System.Security.Cryptography.RSACryptoServiceProvider();

            //公開鍵をXML形式で取得
            publicKey = rsa.ToXmlString(false);
            //秘密鍵をXML形式で取得
            privateKey = rsa.ToXmlString(true);
        }

        /// <summary>
        /// 公開鍵を使って文字列を暗号化する
        /// </summary>
        /// <param name="str">暗号化する文字列</param>
        /// <param name="publicKey">暗号化に使用する公開鍵(XML形式)</param>
        /// <returns>暗号化された文字列</returns>
        public static Option<string> Encrypt(string str, string publicKey)
        {
            //Base64で結果を文字列に変換
            return Encrypt(System.Text.Encoding.UTF8.GetBytes(str), publicKey).Match(
            d => System.Convert.ToBase64String(d).SomeNotNull(), () => Option.None<string>());
        }

        /// <summary>
        /// 公開鍵を使ってバイト列を暗号化する
        /// </summary>
        /// <param name="input">暗号化するバイト列</param>
        /// <param name="publicKey">暗号化に使用する公開鍵(XML形式)</param>
        /// <returns>暗号化されたバイト列</returns>
        public static Option<byte[]> Encrypt(byte[] input, string publicKey)
        {
            //RSACryptoServiceProviderオブジェクトの作成
            System.Security.Cryptography.RSACryptoServiceProvider rsa =
                new System.Security.Cryptography.RSACryptoServiceProvider();
            byte[] encryptedData;
            try
            {
                //公開鍵を指定
                rsa.FromXmlString(publicKey);

                //暗号化する
                //（XP以降の場合のみ2項目にTrueを指定し、OAEPパディングを使用できる）
                encryptedData = rsa.Encrypt(input, false);
            }
            catch
            {
                return Option.None<byte[]>();
            }

            //Base64で結果を文字列に変換
            return encryptedData.SomeNotNull();
        }

        /// <summary>
        /// 秘密鍵を使って文字列を復号化する
        /// </summary>
        /// <param name="str">Encryptメソッドにより暗号化された文字列</param>
        /// <param name="privateKey">復号化に必要な秘密鍵(XML形式)</param>
        /// <returns>復号化された文字列</returns>
        public static Option<string> Decrypt(string str, string privateKey)
        {
            //結果を文字列に変換
            return Decrypt(System.Convert.FromBase64String(str), privateKey).Match(
            d => System.Text.Encoding.UTF8.GetString(d).SomeNotNull(), () => Option.None<string>());
        }

        /// <summary>
        /// 秘密鍵を使って文字列を復号化する
        /// </summary>
        /// <param name="input">Encryptメソッドにより暗号化されたバイト列</param>
        /// <param name="privateKey">復号化に必要な秘密鍵(XML形式)</param>
        /// <returns>復号化されたバイト列</returns>
        public static Option<byte[]> Decrypt(byte[] input, string privateKey)
        {
            //RSACryptoServiceProviderオブジェクトの作成
            System.Security.Cryptography.RSACryptoServiceProvider rsa =
                new System.Security.Cryptography.RSACryptoServiceProvider();
            byte[] decryptedData;
            try
            {
                //秘密鍵を指定
                rsa.FromXmlString(privateKey);

                //復号化する
                decryptedData = rsa.Decrypt(input, false);
            }
            catch
            {
                return Option.None<byte[]>();
            }
            //結果を文字列に変換
            return decryptedData.SomeNotNull();
        }
    }
}
