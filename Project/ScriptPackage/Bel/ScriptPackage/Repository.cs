﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Scripting;
using Optional;

namespace Bel.ScriptPackage
{
    public class Repository
    {
        public bool IsRunning
        {
            get { return scriptTask.Status == TaskStatus.Running; }
            set { if (!value) script.MatchSome(s => s.Exit()); }
        }
        private Option<RepositoryData> data;
        private Option<RepositoryCache> cache
        {
            get { return this.cache; }
            set
            {
                this.cache = value;
                data.MatchSome(d =>
                {
                    var cacheFile = Program.environment.repositoryCacheDirectory
                + d.RepositoryName + ".xml";
                    value.MatchSome(v => Serializer.WriteXML(v,cacheFile));
                });
            }
        }
        private string repoFile;
        private Option<SCPScript> script;
        private Task<ScriptState<object>> scriptTask;

        public Repository(string _repoFile)
        {
            repoFile = _repoFile;
            data = Serializer.ReadXML<RepositoryData>(_repoFile);
            data.MatchSome(d =>
            {
                var cacheFile = Program.environment.repositoryCacheDirectory
                + d.RepositoryName + ".xml";
                cache = File.Exists(cacheFile) ?
                Serializer.ReadXML<RepositoryCache>(cacheFile) : new RepositoryCache().Some();
            });
        }

        public bool Initialize()
        {
            return data.Match(d => cache.Match(c =>
            {
                UpdatePackage(d.User, d.RepositoryName, int.Parse(c.CurrentVersion), d.Key);
                var di = new DirectoryInfo(Program.environment.repositoryDirectory + d.RepositoryName);
                string code = string.Join("\n", di.GetFiles("*.csx").Select(f => File.ReadAllText(f.FullName)));
                code += "\n_Run();";
                var scriptObj = new SCPScript(c,Program.environment.repositoryDirectory + d.RepositoryName + "/");
                script = scriptObj.SomeNotNull();
                var ret = Scripting.RunAsync(code, scriptObj);
                return ret.Match(x => {
                    scriptTask = x.Item1;
                    return true;
                }
                , () => false);
            }, () => false)
            , () => false);
        }

        public bool MakeSCPPackage(string inputDir,string destFile,string privateKey)
        {
            var tmpFile = Program.environment.tempFile;
            ZipFile.CreateFromDirectory(inputDir, tmpFile);
            var enc = CryptoRsa.Encrypt(File.ReadAllBytes(tmpFile), privateKey);
            return enc.Match(d => { File.WriteAllBytes(destFile, d); return true; }, () => false);
        }

        private static bool UpdatePackage(string userName,string repository,int currentVersion, string key)
        {
            var newRev = Updater.CheckUpdate(userName, repository, currentVersion);
            if (!newRev.HasValue)
                return false;
            var downloadFile = Program.environment.tempFile;
            Updater.Download(userName, repository, newRev.ValueOr(currentVersion), downloadFile);
            ZipFile.ExtractToDirectory(downloadFile, downloadFile + "-ext");
            var di = new DirectoryInfo(downloadFile + "-ext");
            var fi = di.GetFiles("release.scp",SearchOption.AllDirectories);
            if (fi.Count() <= 0)
                return false;
            var dec = CryptoRsa.Decrypt(File.ReadAllBytes(fi[0].FullName), key);
            if (!dec.HasValue)
                return false;
            File.WriteAllBytes(downloadFile + "-dec", dec.ValueOr(new byte[] { }));
            ZipFile.ExtractToDirectory(downloadFile + "-dec", downloadFile + "-decext");
            var di2 = new DirectoryInfo(Program.environment.repositoryDirectory + repository);
            MyFile.CopyDirectory(Program.environment.repositoryDirectory + repository + "/save/"
            , downloadFile + "-decext/scp/", true);
            MyFile.DeleteDirectory(Program.environment.repositoryDirectory + repository);
            MyFile.CopyDirectory(downloadFile + "-decext/scp/",
                Program.environment.repositoryDirectory + repository, true);
            return true;
        }

        private Repository() { }

        public static Repository MakeRepository(string _repoFile,string repoName,string user,string key)
        {
            Repository repo = new Repository();
            repo.repoFile = _repoFile;
            repo.data = new RepositoryData() { RepositoryName = repoName, Key = key, User = user }.Some();
            repo.data.MatchSome(value => Serializer.WriteXML(value, repo.repoFile));
            repo.data.MatchSome(d =>
            {
                var cacheFile = Program.environment.repositoryCacheDirectory
                + d.RepositoryName + ".xml";
                repo.cache = File.Exists(cacheFile) ?
                Serializer.ReadXML<RepositoryCache>(cacheFile) : new RepositoryCache().Some();
            });
            return repo;
        }

        private static Option<(Task<ScriptState<object>> task, SCPScript scriptObject)> LoadScript(string workingDirectory, SCPScript scriptObject)
        {
            var di = new DirectoryInfo(workingDirectory);
            var codeArray = di.GetFiles("*.csx").Select(f =>
                File.ReadAllText(f.FullName)).ToArray();
            var code = string.Join("\n", codeArray);
            return Scripting.RunAsync(code, scriptObject);
        }

        private static Option<string> DecryptSCP(string scpFile, string key)
        {
            var tempFile = Program.environment.tempFile;
            var encryptoData = File.ReadAllBytes(scpFile);
            return CryptoRsa.Decrypt(encryptoData, key).Match(
                d =>
                {
                    File.WriteAllBytes(tempFile + ".zip", d);
                    return (tempFile + ".zip").Some();
                },
                () =>
                {
                    return Option.None<string>();
                });
        }
    }

    [System.Xml.Serialization.XmlRoot("Repository")]
    public class RepositoryData
    {
        [System.Xml.Serialization.XmlElement("BitBucketUser")]
        public String User { get; set; }

        [System.Xml.Serialization.XmlElement("BitBucketRepository")]
        public String RepositoryName { get; set; }

        [System.Xml.Serialization.XmlElement("Key")]
        public String Key { get; set; }
    }

    [System.Xml.Serialization.XmlRoot("RepositoryCache")]
    public class RepositoryCache
    {
        [System.Xml.Serialization.XmlElement("CurrentVersion")]
        public String CurrentVersion { get; set; }

        [System.Xml.Serialization.XmlElement("InitializedTasks")]
        public List<String> InitializedTasks { get; set; }
    }
}
