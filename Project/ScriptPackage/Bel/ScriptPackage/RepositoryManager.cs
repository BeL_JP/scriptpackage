﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Optional;

namespace Bel.ScriptPackage
{
    public sealed class RepositoryManager
    {
        private List<Repository> repositoryList = new List<Repository>();
        private static RepositoryManager Instance = new RepositoryManager();
        private RepositoryManager()
        {
            ReadAllRepository().MatchSome(repos =>
            {
                repositoryList.AddRange(repos);
            });
        }

        public static RepositoryManager GetInstance()
        {
            return Instance;
        }

        private static Option<Repository[]> ReadAllRepository()
        {
            var repositoryDirectory = Program.environment.repositoryDirectory;
            var di = new DirectoryInfo(repositoryDirectory);
            if (!di.Exists)
                return new Repository[] { }.None();
            var fi = di.GetFiles("*.scprepo");
            var repos = fi.Select(f => new Repository(f.FullName)).ToList();
            return fi.Select(f => new Repository(f.FullName)).ToArray().SomeNotNull();
        }
    }
}
