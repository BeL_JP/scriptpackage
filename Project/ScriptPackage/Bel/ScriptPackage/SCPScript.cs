﻿using System;
using System.Collections.Generic;

namespace Bel.ScriptPackage
{
    public class SCPScript
    {
        public class Info{
            public string Name = "noname";
            public int Revision = 1;
            public string Description = "no description.";
        }
        public Info info
        {
            get { return _info; }
        }
        public readonly string workingDirectory;
        private Info _info = new Info();
        private static List<IAct> actionList = new List<IAct>();
        private RepositoryCache cache;
        private System.Threading.Thread t;

        public SCPScript()
        {

        }

        public SCPScript(RepositoryCache _cache, string _workingDirectory)
        {
            workingDirectory = _workingDirectory;
            cache = _cache;
        }

        public void Exit()
        {

        }

        public void _Run()
        {
            List<Action> taskList = new List<Action>();
            while (true)
            {
                int taskCount = taskList.Count;
                foreach (var actObj in actionList)
                {
                    var requireOK = true;
                    foreach (var req in actObj.GetRequire())
                    {
                        if (!cache.InitializedTasks.Contains(req))
                        {
                            requireOK = false;
                            break;
                        }
                    }
                    if (!requireOK)
                        continue;
                    var act = actObj.GetAction();
                    if (actObj is Act)
                    {
                        taskList.Add(act);
                        cache.InitializedTasks.Add(actObj.GetTaskName());
                    }
                    else if (actObj is ActOnce)
                    {
                        if (!cache.InitializedTasks.Contains(actObj.GetTaskName()))
                        {
                            taskList.Add(act);
                            cache.InitializedTasks.Add(actObj.GetTaskName());
                        }
                    }
                }
                if(taskCount != taskList.Count)
                {
                    continue;
                }
                else
                {
                    t = new System.Threading.Thread(new System.Threading.ThreadStart(() => {
                        foreach(var task in taskList)
                        {
                            task();
                        }
                    }));
                    t.Start();
                }
            }
            

        }

        public void SetPackageInfo(string packageName, int packageRevision, string description)
        {
            _info.Name = packageName;
            _info.Revision = packageRevision;
            _info.Description = description;
        }

        public interface IAct
        {
            IAct Require(params string[] req);
            void Begin(Action act);
            Action GetAction();
            string[] GetRequire();
            string GetTaskName();
        }

        public class Act : IAct
        {
            public readonly string taskName;
            private string[] _require;
            protected Action _action;
            public Act(string _taskName)
            {
                taskName = _taskName;
            }

            public void Begin(Action act)
            {
                _action = act;
                actionList.Add(this);
            }

            public IAct Require(params string[] req)
            {
                _require = req;
                return this;
            }

            public Action GetAction()
            {
                return _action;
            }

            public string GetTaskName()
            {
                return taskName;
            }

            public string[] GetRequire()
            {
                return _require;
            }
        }

        public class ActOnce : Act, IAct
        {
            public ActOnce(string _taskname) : base(_taskname) { }
            public new void Begin(Action act)
            {
                _action = act;
                actionList.Add(this);
            }
            public new IAct Require(params string[] req)
            {
                return (ActOnce)base.Require(req);
            }
        }
    }
}
