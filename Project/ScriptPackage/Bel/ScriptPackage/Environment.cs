﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Scripting;

namespace Bel.ScriptPackage
{
    public sealed class Environment
    {
        public string tempDirectory
        {
            get
            {
                var newDir = tempBaseDirectory + System.Guid.NewGuid().ToString("N");
                Directory.CreateDirectory(newDir);
                return newDir;
            }
        }
        public string tempFile
        {
            get
            {
                return tempBaseDirectory + System.Guid.NewGuid().ToString("N");
            }
        }
        public readonly string environmentDirectory = @"env/";
        public readonly string repositoryCacheDirectory = @"repocache/";
        public readonly string repositoryDirectory = @"repos/";

        private static readonly string tempBaseDirectory = @"temp/";
        private static Environment Instance = new Environment();

        private Environment()
        {
            if (Directory.Exists(tempDirectory))
                MyFile.DeleteDirectory(tempDirectory);
            Directory.CreateDirectory(tempDirectory);
            if (!Directory.Exists(environmentDirectory))
                Directory.CreateDirectory(environmentDirectory);
            if (!Directory.Exists(repositoryCacheDirectory))
                Directory.CreateDirectory(repositoryCacheDirectory);
            if (!Directory.Exists(repositoryDirectory))
                Directory.CreateDirectory(repositoryDirectory);

        }

        public static Environment GetInstance()
        {
            return Instance;
        }
    }
}
