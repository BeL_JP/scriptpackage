﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Bel.ScriptPackage
{
    public interface IMode
    {
        void Initialize();
        void Task();
    }

    public class ConfigMode : IMode
    {
        public void Initialize()
        {
            Updater.SelfUpdate("BeL_JP", "ScriptPackage", Program.REVISION);
        }

        public void Task()
        {
            
        }
    }

    public class BackgroundMode : IMode
    {
        public void Initialize()
        {
            Updater.SelfUpdate("BeL_JP", "ScriptPackage", Program.REVISION);
        }

        public void Task()
        {
            List<Repository> repos = new List<Repository>();
            var di = new DirectoryInfo(Program.environment.repositoryDirectory);
            var fi = di.GetFiles("*.scprepo");
            foreach (var f in fi)
            {
                var repo = new Repository(f.FullName);
                repos.Add(repo);
                repo.Initialize();
            }
            while (true)
            {
                bool exit = true;
                foreach (var repo in repos)
                {
                    if (repo.IsRunning)
                        exit = false;
                }
                if (exit)
                    break;
            }
        }
    }

    public class MakingMode : IMode
    {
        public void Initialize()
        {
            Updater.SelfUpdate("BeL_JP", "ScriptPackage", Program.REVISION);
        }

        public void Task()
        {
            
        }
    }
}
