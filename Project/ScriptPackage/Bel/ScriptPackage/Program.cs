﻿#define DEBUG
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bel.ScriptPackage
{
    public class Program
    {
        public const int REVISION = 1;
        public readonly static Environment environment = Environment.GetInstance();
        public readonly static CommandLineInterface cli = CommandLineInterface.GetInstance();
        static void Main(string[] args)
        {
            cli.AddOption("mode");
            cli.AddOption("key");
            cli.Parse();
            IMode mode;
            if (cli.options.ContainsKey("mode"))
            {
                mode = GetMode(cli.options["mode"]);
            }
            else
            {
                mode = GetMode("background");
            }

            mode.Initialize();
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(() => mode.Task()));
            t.Start();
            while (t.IsAlive)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        private static IMode GetMode(string mode)
        {
            switch (mode)
            {
                case "config": return new ConfigMode();
                case "background": return new BackgroundMode();
                case "making": return new MakingMode();
                default: return new BackgroundMode();
            }
        }
    }
}

