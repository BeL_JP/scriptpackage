﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using Optional;

namespace Bel
{
    public struct Scripting
    {
        public static Option<(Task<ScriptState<object>> task, T scriptObject)> RunAsync<T>(string code,T _scriptObject)
            where T : new()
        {
            ScriptOptions options = ScriptOptions.Default
                                                 .WithReferences(Assembly.GetAssembly(typeof(T)), Assembly.GetEntryAssembly());
            T scriptObject;
            Task<ScriptState<object>> task;
            try
            {
                var script = CSharpScript.Create(code, options, typeof(T));
                task = script.RunAsync(scriptObject = _scriptObject);
            }
            catch
            {
                return Option.None<(Task<ScriptState<object>> task, T scriptObject)>();
            }
            return (task, scriptObject).SomeNotNull();
        }

        public static Option<(Task<ScriptState<object>> task, T scriptObject)> RunAsync<T>(string fileName, Encoding encoding,T _scriptObject)
            where T : new()
        {
            string code = File.ReadAllText(fileName, encoding);
            return RunAsync(code, _scriptObject);
        }

        public static Option<(Task<ScriptState<object>> task, T scriptObject)> RunAsync<T>(string fileName, Encoding encoding)
            where T : new()
        {
            string code = File.ReadAllText(fileName, encoding);
            return RunAsync<T>(code);
        }

        public static Option<(Task<ScriptState<object>> task, T scriptObject)> RunAsync<T>(string code)
            where T : new()
        {
            return RunAsync(code, new T());
        }
    }
}
